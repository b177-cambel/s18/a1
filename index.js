let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function() {
		console.log("Pikachu, ! I choose you!")
	}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();

function Pokemon(name, level, attack) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = attack;

	this.tackle = function(target) {
		let attackedHealth = (target.health - this.attack);
		console.log(this.name + " tackled " + target.name + ".");
		console.log(target.name + "'s health is now reduced to " + attackedHealth + ".");
		console.log(target);

		if (attackedHealth <= 0) {
			console.log(target.name + " fainted.")
		};
	};
};

let pikachu = new Pokemon("Pikachu", 42, 20);
	snorlax = new Pokemon("Snorlax", 24, 12);
	charmainder = new Pokemon("Charmainder", 60, 55);

console.log(pikachu);
console.log(snorlax);
console.log(charmainder);

snorlax.tackle(pikachu);
pikachu.tackle(charmainder);
charmainder.tackle(snorlax);